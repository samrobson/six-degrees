# Six Degrees

Find how many degrees of separation exist between two URLs, where a degree of separation is a link

For example, there is one degree separation between `https://stackoverflow.com/` and `hhttps://mathoverflow.net/`, as there is a link in the footer of the former

This was mostly an excuse to learn more about distributed systems and Go...


## How to Run
`docker-compose up --scale worker=n` to start the supervisor, NATS, and redis containers, alongside `n` workers

Send get requests with mandatory "from" and optional "to" query parameters to localhost:8080, e.g.

`curl http://localhost:8080/?from=https%3A%2F%2Fgoogle.com&to=https%3A%2F%2Fyoutube.com
`

## Design
![Design](SixDegreesArchitecture.png "Six Degrees Architecture")


## Supervisor
- Accepts GET requests at port 8080
- Parses the request, expecting 'from' and allowing 'to' query parameters
- Publishes a message to the `urls` subject on the NATS server 
- Waits for the appropriate NATS subject (name determined by the URLs given) to receive a message, before returning the results as JSON to the client

## Worker
- Picks messages from a NATS queue group
- The messages contain the next url to scan, alongside contextual information
- Verify via Redis that the job that involves the current URL is still active, and that the URL has not been scanned as part of this job
- Send a GET request to the specified url
- Parse the resultant HTML for `href` links
    - If these contain the one we are looking for, publish a message to alert the supervisor, and save the finished state to Redis
    - Otherwise, publish a new message to the `urls` subject containing the fresh url, if it has not yet been visited

## Redis
- Cache to allow workers to avoid processing URLs that have already been processed, or for those that pertain to jobs that are already complete

## NATS
- Cloud messaging server, allowing workers to publish urls to a queue for others to draw from
- 
