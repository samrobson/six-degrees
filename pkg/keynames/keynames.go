package keynames

import (
	"fmt"
	"net/url"
)

// SearchStatusKey returns the key for the job involving the two specified URLs
func SearchStatusKey(fromURL string, toURL string) string {
	from, err := url.Parse(fromURL)
	if err != nil {
		printParseError(fromURL, err)
	}
	to, err := url.Parse(toURL)
	if err != nil {
		printParseError(toURL, err)
	}

	return fmt.Sprintf("%s-%s", from.Host, to.Host)
}

// URLVisitedStatusKey returns the key used to determine whether the currentURL has been visited for this job
func URLVisitedStatusKey(fromURL string, toURL string, currentURL string) string {
	current, err := url.Parse(currentURL)
	if err != nil {
		printParseError(currentURL, err)
	}
	return fmt.Sprintf("%s-%s", SearchStatusKey(fromURL, toURL), current.Host)
}

func printParseError(u string, err error) {
	fmt.Printf("Error parsing %s : %s", u, err)
}
