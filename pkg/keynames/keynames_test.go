package keynames

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetJobStatusKey(t *testing.T) {
	expected := "google.com-youtube.com"
	actual := SearchStatusKey("https://google.com", "http://youtube.com")

	assert.Equal(t, actual, expected)
}

func TestGetURLVisitedKey(t *testing.T) {
	expected := "google.com-youtube.com-bbc.co.uk"
	actual := URLVisitedStatusKey("https://google.com", "http://youtube.com", "https://bbc.co.uk")

	assert.Equal(t, actual, expected)
}
