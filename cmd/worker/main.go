package main

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"os"

	"github.com/PuerkitoBio/goquery"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
	"github.com/golang/protobuf/proto"
	"github.com/nats-io/nats.go"
	"gitlab.com/samrobson/six-degrees/pkg/keynames"
	"gitlab.com/samrobson/six-degrees/pkg/transport"
)

type cfg struct {
	context context.Context
	conn    *nats.Conn
	redis   *redis.Client
}

var config cfg

func main() {
	config.context = context.Background()
	// Connect to NATS
	var err error
	natsURI := os.Getenv("NATS_URI")
	config.conn, err = nats.Connect(natsURI)
	if err != nil {
		fmt.Println(err)
	}

	// Connect to Redis
	redisURI := os.Getenv("REDIS_URI")
	config.redis = redis.NewClient(&redis.Options{
		Addr:     redisURI,
		Password: "",
		DB:       0,
	})

	if _, err = config.conn.QueueSubscribe("urls", "workers", handleIncomingURL); err != nil {
		fmt.Println(err)
	}
	fmt.Println("Worker subscribed")

	// Health check endpoint
	r := gin.Default()
	r.GET("/", health)
	r.Run(":8181")
}

// Message handler responsible for requesting and parsing HTML, and processing any links within
func handleIncomingURL(msg *nats.Msg) {
	search := transport.Search{}
	err := proto.Unmarshal(msg.Data, &search)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("Picked %s from the queue\n", search.Next)

	// If the job has finished or the URL has been visited (for this job), abort
	if haveResult(search.Start, search.End) || urlVisited(search.Start, search.End, search.Next) {
		return
	}

	fmt.Printf("Getting %s\n", search.Next)
	res, err := http.Get(search.Next)
	if err != nil {
		// Some href attributes are not valid URLs, just move on if we encounter one
		fmt.Println(err)
		return
	}

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		fmt.Println(err)
	}
	// Parse the HTML for links, and process them
	doc.Find("a[href]").Each(func(i int, item *goquery.Selection) {
		u, _ := item.Attr("href")
		parsed, err := url.Parse(u)
		if err != nil {
			fmt.Println(err)
			return
		}
		if parsed.Scheme == "http" || parsed.Scheme == "https" {
			processURL(search.Start, search.End, search.Next, search.Visited, u)
		}
	})
	// Mark that the url has been visited
	e := config.redis.Set(config.context, keynames.URLVisitedStatusKey(search.Start, search.End, search.Next), true, 0).Err()
	if e != nil {
		fmt.Println(err)
	}
}

// Check whether this job is ongoing
func haveResult(fromURL string, toURL string) bool {
	result := config.redis.Exists(config.context, keynames.SearchStatusKey(fromURL, toURL))
	return result.Val() == 1
}

// Check whether the URL has been visited as part of this job
func urlVisited(fromURL string, toURL string, u string) bool {
	result := config.redis.Exists(config.context, keynames.URLVisitedStatusKey(fromURL, toURL, u))
	return result.Val() == 1
}

// Publish URLs if they have not been visited before, update state if the right one has been found
func processURL(start string, end string, current string, visited []string, newURL string) {
	// We are only interested in host names, not full paths
	parsed, err := url.Parse(newURL)
	next := parsed.Scheme + "://" + parsed.Host

	updatedSearch := transport.Search{
		Start:   start,
		End:     end,
		Next:    next,
		Visited: append(visited, current),
	}

	data, err := proto.Marshal(&updatedSearch)
	if err != nil {
		fmt.Println(err)
		return
	}

	// Ff this is the URL we are looking for
	if equivalentUrls(next, end) {
		fmt.Printf("Got it! Found %s at %s\n", next, current)
		// Publish message to subject for the supervisor to pick up
		if err = config.conn.Publish(keynames.SearchStatusKey(start, end), data); err != nil {
			fmt.Println(err)
		}
		// Save finished state in Redis, to avoid processing unnecessary URLs in the queue
		if err := config.redis.Set(config.context, keynames.SearchStatusKey(start, end), true, 0).Err(); err != nil {
			fmt.Println(err)
		}
		return
	}

	// If the URL has yet to be visited, publish a new message to the queue
	if !urlVisited(start, end, next) {
		if err = config.conn.Publish("urls", data); err != nil {
			fmt.Println(err)
		}
	}
}

// Checking hosts only
func equivalentUrls(url1 string, url2 string) bool {
	url1Parsed, err := url.Parse(url1)
	if err != nil {
		fmt.Print(err)
	}
	url2Parsed, err := url.Parse(url2)
	if err != nil {
		fmt.Print(err)
	}
	return url1Parsed.Host == url2Parsed.Host
}

func health(context *gin.Context) {
	context.String(200, "OK")
}
