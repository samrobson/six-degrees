package main

import (
	"fmt"
	"os"
	sync "sync"

	"github.com/gin-gonic/gin"
	"github.com/golang/protobuf/proto"
	"github.com/nats-io/nats.go"
	"gitlab.com/samrobson/six-degrees/pkg/keynames"
	"gitlab.com/samrobson/six-degrees/pkg/transport"
)

var kevinBaconURL string = "https://oracleofbacon.org/"

type cfg struct {
	conn *nats.Conn
}

// JSON to return to the client
type degrees struct {
	From  string   `json:"from"`
	To    string   `json:"to"`
	Steps int      `json:"steps"`
	Urls  []string `json:"urls"`
}

var config cfg

func main() {
	// Connect to NATS
	var err error
	natsURI := os.Getenv("NATS_URI")
	config.conn, err = nats.Connect(natsURI)
	if err != nil {
		fmt.Println(err)
	}

	r := gin.Default()
	r.GET("/", func(ginContext *gin.Context) {
		cp := ginContext.Copy()
		result := make(chan degrees)
		var wg sync.WaitGroup
		wg.Add(1)
		go func() {
			defer wg.Done()
			handleGet(cp, result)
		}()
		wg.Wait()
		ginContext.JSON(200, <-result)
	})
	r.Run(":8080")
}

// Publish to NATS subject following the GET request, sending the result to the passed channel
func handleGet(ginContext *gin.Context, result chan degrees) {
	fromURL := ginContext.Query("from")
	toURL := ginContext.Query("to")
	if toURL == "" {
		toURL = kevinBaconURL
	}

	search := transport.Search{
		Start:   fromURL,
		End:     toURL,
		Next:    fromURL,
		Visited: []string{},
	}

	data, err := proto.Marshal(&search)
	if err != nil {
		fmt.Println(err)
	}

	// Publish the message for the workers to pick up
	config.conn.Publish("urls", data)
	_, err = config.conn.Subscribe(keynames.SearchStatusKey(fromURL, toURL), func(msg *nats.Msg) {
		handleReturn(msg, result)
	})
	if err != nil {
		fmt.Println(err)
	}
}

func handleReturn(msg *nats.Msg, result chan degrees) {
	searchResult := transport.Search{}
	if err := proto.Unmarshal(msg.Data, &searchResult); err != nil {
		fmt.Println(err)
	}
	result <- degrees{
		searchResult.Start,
		searchResult.End,
		len(searchResult.Visited),
		searchResult.Visited,
	}
}
